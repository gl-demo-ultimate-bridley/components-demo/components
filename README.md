# Components for working with GoLang
This CI/CD component provides useful utilities for working with the Go programming language.

## Functionalities Included:
- Testing
- Building
- Deployment
- Secure Container Builds
- ... More to come.

